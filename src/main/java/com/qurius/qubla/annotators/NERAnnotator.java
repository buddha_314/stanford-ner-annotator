package com.qurius.qubla.annotators;

import com.qurius.uima.annotations.NER;
import com.qurius.uima.annotations.Phrase;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.descriptor.*;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

import java.util.List;

import static org.apache.uima.fit.util.JCasUtil.selectCovered;

/**
 * Created by buddha on 10/29/15.
 */
@ResourceMetaData(
        name = "7Class NER",
        description = "From the Stanford NLP Group, this Annotator identifies people, locations, institutions and other signification entities from text",
        vendor = "Stanford NLP Group",
        version = "1.7.0",
        copyright = "GPL v2+"
)
@OperationalProperties(
        modifiesCas = true,
        outputsNewCases = false
)
@TypeCapability(
        outputs = { "com.qurius.uima.annotations.NER"}
)
public class NERAnnotator extends org.apache.uima.fit.component.JCasAnnotator_ImplBase {

    private AbstractSequenceClassifier<CoreLabel> classifier;

    public static final String PARAM_SKIP = "skipIfPreviousAnnotation";
    @ConfigurationParameter(name = PARAM_SKIP, mandatory = false, description = "TRUE means the Annotator will look for entities in previous annotations.  FALSE means it will only apply to the document text.")
    private boolean skipIfPreviousAnnotation;

    public static final String PARAM_CLASSIFIER_PATH = "classifierPath";
    @ConfigurationParameter(name = PARAM_CLASSIFIER_PATH, mandatory = false, description = "Location of the trained model file.")
    private String classifierPath;

    @Override
    public void initialize(UimaContext aContext) throws ResourceInitializationException {
        super.initialize(aContext);
        if (classifierPath != null) {
            classifier = CRFClassifier.getClassifierNoExceptions(classifierPath);
        } else {
            throw new RuntimeException("classifierPath not set");
        }
    }

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {
        String document = aJCas.getDocumentText();
        List<Triple<String, Integer, Integer>> triplesList = classifier.classifyToCharacterOffsets(document);
        for (Triple<String, Integer, Integer> entity : triplesList) {
            String type = entity.first;
            int start = entity.second;
            int end = entity.third;
            String phrase = document.substring(start, end);
            if (skipIfPreviousAnnotation) {
                // skip if named entity is already covered by an annotation of the same length
                boolean foundAnnotation = false;
                List<Phrase> longerAnnotations = selectCovered(aJCas, Phrase.class, start, end);
                for (Phrase p : longerAnnotations) {
                    if (p.getCoveredText().contains(phrase)) {
                        foundAnnotation = true;
                        break;
                    }
                }
                if (foundAnnotation) {
                    continue;
                }
            }
            NER annotation = new NER(aJCas);
            annotation.setBegin(start);
            annotation.setEnd(end);
            annotation.setNERType(type);
            annotation.addToIndexes();
        }

    }
}


