package com.qurius.qubla.annotators;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by buddha on 11/5/15.
 */
public class NERAnnotatorTest {
    private static String YAML_FILE = ".qubla.yml";
    private String DOCUMENT_TEXT;
    private String DOCUMENT_LANGUAGE;
    AnalysisEngineDescription aed;
    Object[] paramsArray;

    @Before
    public void setUp() {

    }

    @Test
    public void testItAllLetJUnitSortItOut() throws IOException, UIMAException {
        readYamlConfigurePipeline();
        JCas jCas = JCasFactory.createJCas();
        jCas.setDocumentText(DOCUMENT_TEXT);
        jCas.setDocumentLanguage(DOCUMENT_LANGUAGE);

        aed = AnalysisEngineFactory.createEngineDescription(NERAnnotator.class, paramsArray);
        aed.doFullValidation();

        SimplePipeline.runPipeline(jCas, new AnalysisEngineDescription[]{aed});

        /* Check the Annotations */
        /* Final score should be 5 + 6 + 2 - 3 = 10 */
        FSIterator iterator = jCas.getAnnotationIndex().iterator();
        while (iterator.hasNext()) {
            Annotation annotation = (Annotation) iterator.next();
            System.out.print("\n *** " + annotation.getCoveredText() + "\n");
            System.out.print(annotation.toString());
        }
    }

    private void readYamlConfigurePipeline() throws IOException {
        Yaml yaml = new Yaml();
        String yamlString = FileUtils.readFileToString(new File(YAML_FILE));
        Map<String, Object> config = (Map<String, Object>) yaml.load(yamlString);

        // Set the document text
        Map<String, Object> doc = (Map<String, Object>)config.get("test-document");
        DOCUMENT_TEXT = (String) doc.get("documentText");
        DOCUMENT_LANGUAGE = (String) doc.get("language");

        // Get the Annotator paramters
        Map<String, Object> params = (Map<String, Object>) config.get("parameters");
        paramsArray = new Object[2 * params.size()];
        int i = 0;
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            Map<String, Object> v = (Map<String, Object>) entry.getValue();
            paramsArray[i] = entry.getKey();
            if (v.containsKey(("isFile")) && (Boolean)v.get("isFile")) {
                paramsArray[(i + 1)] = new File((String) v.get("defaultValue")).getAbsolutePath();
                //System.out.println("Got a boolean!");
            } else {
                paramsArray[(i + 1)] = v.get("defaultValue");
            }
            i += 2;
        }
    }
}